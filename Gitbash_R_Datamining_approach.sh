#Script datamining Vvul
# The workflow is:
# 1. Selection of downloads from ENA
# 2. Applying filters on metadata preselect runs to process (DataMining_dataCheck_GlobalVulnificus_3 (part 1) line:1 - 239)
# 3. Download unique links Gitbash_R_Datamining_approach.sh (Section A )
# 4. Trim premerged files and merge and trim unmerged files (Snakefile_trim_merged_single_files and Snakefile_trim_merge_paired_files)
# 5. A filter is applied to remove merged files that have less than 10000 merged reads
# 6. Quality information is removed and a vvul blast DB is made using makeblastdb (Section C)
# 7. premerged and newly merged files are compared to the blastdb in snakmake (Snakefile_megablast)
# 8. The blast output is parsed and runs that don't have 3 hits at minimum 95% are discarded (Section D) (there is a normal line and remerged line here - this split is inconsequential and due to an earlier mixup where files where included and excluded that
# shouldn't have been. This has been remedied and the correct files are used in the continuation of the processing.
# 9. All sequences in the remaining files are aligned to the global sina alignment to find common start and end positions
# 10. The most common start and end position of every file are collected in one file in (Section E)
# 11. The most common start/end positions are compared, resulting in 3 clusters (V3_V4 - V4 and V4_V5)
# In these clusters vvul can be distinguished from other vibrio and based on the most frequent start and end position, primers will be cut (DataMining_dataCheck_GlobalVulnificus_3 (part 1) line: 242 - 350)
# 12. "6427_23438.txt" / "13861_23438.txt" / "13861_27656.txt" contain the runs with the corrosponding start and end positions. Aligned sequenced are cut to common start/end per defined sequence (Section F)
# 13. Blast DB against itself to check for any potential misannotation in refseq sequences (Section G + DataMining_dataCheck_GlobalVulnificus_3 Part 2.5)
# 15. Unoise is used for denoising - follow Unoise recommended approach: denoise only dominant sequences and then map reads back to zotus to create OTU table (Section H)
# 16. OTU tables are parsed and taxonomy is determined in otu_parse_taxonomy.R
# 17. Plots and ML is performed in Global_vul_rescue.R


#Section A
# Create unique_links download links from R script DataMining_dataCheck_GlobalVulnificus_3 (part 1)
# unique_links uploaded to IOWseq000045_BaltVib/

sed 's/^/http:\/\//' unique_links.txt > links.txt #present
# Create detached screen and download
screen -S download
conda activate aria2-1.34.0

# If download fails, rerun aria2c command, makes check for existing files. 
aria2c -i ../links.txt -c --dir . --max-tries=20 --retry-wait=5 --max-connection-per-server=1 --max-concurrent-downloads=50 &>> ../download.log

# extract sample list from file names
cd downloads #removed for space reasons
ls -1 | cut -d'_' -f1 | sed 's/\.fastq\.gz//' | sort | uniq > ../sample_list.txt #present

# Switch to snakemake #
# There are pre-merged (sample_list1) and unmerged files (sample_list2), these are processed seperately in 
# Snakefile_trim_merged_single_files and Snakefile_trim_merge_paired_files respectively

#Section B
# remove merge columns with less than 10000 merged from the paired logs
grep "Joined" *_merged.log | sed 's/ //g' | awk '$2 >= 10000' | sed 's/%//' > ../merged_reprocess_filtered.txt

# Clean up first column 
sed -i 's/_merged.log:Joined://g' merged_reprocess_filtered.txt

# remove premerged files with insufficient merged reads 
riedinge@bio-40:/bio/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/Vvul_data_mining/merged_files2$ grep "Result" *_trimming.log | head | hexdump -c

grep "Result" *_trimming.log | sed 's/ //g' | sed 's/reads(/\t/' |  sed 's/%).*//' | awk '$2 >= 10000'   >> ../samplelist1_filtered.txt
sed -i 's/_trimming.log:Result://g' samplelist1_filtered.txt

#Section C
# Now we have trimmed and merged files. We can remove quality information by turning fastq into fasta using bbtools reformat.sh of only the files that passed quality chec in merged_reprocess_filtered
cut -d'_' -f1 merged_reprocess_filtered.txt | parallel -j40 'reformat.sh in=merged_files2/{}.fastq.gz out=fasta_files/{}.fa fastawrap=10000 threads=2'

# It's MEGABLAST time! # prepare vvul database for blast
conda activate blast-2.13.0

makeblastdb -in vvul_database/16S_rRNA_vvul_customdb.fasta -dbtype nucl -out vvul_database/16S_rRNA_vvul_customdb -logfile vvul_database/16S_rRNA_vvul_customdb.log

# Switch to snakemake #
# Run Snakefile_megablast for blast, input list is sample_list_blast which is a combination of both premerged and previously unmerged files
# Within Snakefile_megablast, the 95% alignment and match filter for at minimum 3 reads is not implemented. This is done here to remove 18S/ITS etc. amplifications

# Section D
# For the remerged files in this script/ the exact same is done for blast_filt. This is the result of an earlier splitting of files and has no consequences.
# This split leads to selected_runs and selected_runs_remerged, the newly added files are in selected_runs_remerged_new.txt
mkdir blast_filtered_remerged
ls blast_out_remerged | xargs -n1 basename | sed 's/\_blast_out.txt//' | while read line
do
  awk '$3 >= 95 && $13 >= 95' blast_out_remerged/${line}_blast_out.txt  > blast_filtered_remerged/${line}_blast_out_filt.txt
done

find blast_filtered_remerged/ -name '*_blast_out_filt.txt' -print0 | xargs -0 wc -l | grep -v "insgesamt" | sed -r 's/^ +//' | sed -e 's/ blast_filtered_remerged\//\t/' -e 's/_blast_out_filt.txt$//' | awk '$1 >= 3' > selected_runs_remerged_new.txt

# keep only the 2nd column with the run_accession, the first column contains the amount of hits
awk '{print $2}' selected_runs_remerged_new.txt > temp.txt && mv temp.txt selected_runs_remerged_new.txt

# Runs are selected that pass the filter and are used for further processing
wc -l blast_filtered_fasta_files/*_blast_out_filt.txt | grep -v "insgesamt" | sed -r 's/^ +//' | sed -e 's/ blast_filtered_fasta_files\//\t/' -e 's/_blast_out_filt\.txt//' | awk '$1 >= 3' > selected_runs.txt

cut -f2 selected_runs.txt | while read line
do
	cut -f14 blast_filtered_fasta_files/${line}_blast_out_filt.txt | sort -k1,1g | uniq -c | sed -e 's/^ *//' -e 's/ /\t/' | sort -k1,1g | tail -1 | paste <(echo $line) - 
done > fraglength.txt 

# Switch to snakemake #
# A global alignment is made with Snakefile_sina. The log files are parsed to get most common start and end position of the reads in the SINA alignment
# all these start and end positions are in /parsed

#Section E
# The start and end position is collected in one file for further processing in R using the following command
ls -1 | parallel -j20 "cut -f2,10,11 {} | sed 's/: /\t/g' | cut -f2,4,6 | sed 's/\./\t/' | cut -f1,3,4 | sort | uniq -c | sed -e 's/^ *//' -e 's/ /\t/' | sort -k1,1g | tail -1" > ../aligned_start_end.txt
ls -1 | parallel -j20 "cut -f2,10,11 {} | sed 's/: /\t/g' | cut -f2,4,6 | sed 's/\./\t/' | cut -f1,3,4 | sort | uniq -c | sed -e 's/^ *//' -e 's/ /\t/' | sort -k1,1g | tail -1" > ../aligned_start_end_remerged_new.txt


# This is used in part 2 of R script DataMining_dataCheck_GlobalVulnificus_3 to identify clusters of common start and end positions that cover the regions where
# Vulnificus differs from closely related sister branches and species

# Section F
# Set txt_file to the name of the file you want to process # Here V3_V4
conda activate seqtk-1.3
txt_file="6427_23438.txt"

# Extract the numbers from the filename
IFS='_' read -ra AR <<< "$txt_file"
num1=${AR[0]}
num2=$((50000 - ${AR[1]%.txt}))

# Create a subdirectory under aligned_cut with the txt_file indication
subdir="aligned_cut/${txt_file%.txt}"
mkdir -p "$subdir"

# Use process substitution with parallel to read each line in the text file
cat "$txt_file" | parallel -j 36 "seqtk trimfq -b ${num1} -e ${num2} aligned/{}_aligned.fasta.gz | sed -e '/^>/s/ .*$//' -e '/^>/s/\./_/' -e '/^>/! s/U/T/g' -e '/^>/! s/-//g' > aligned_cut/V3_V4/{}_aligned_cut.fa" 

#apply a length filter to remove bad sequences
conda activate seqkit-2.3.1 
cat V3_V4/*.fa | seqkit seq -m 350 -M 410 > filtered_combined_${num1}_${num2}.fa

# Set txt_file to the name of the file you want to process #### TXT FILE 13861_23438.txt #### This is a repetition of the process above for a different primer cluster
txt_file2="13861_23438.txt"

# Extract the numbers from the filename
IFS='_' read -ra AR <<< "$txt_file2"
num3=${AR[0]}
num4=$((50000 - ${AR[1]%.txt}))


# Use process substitution with parallel to read each line in the text file
cat "$txt_file2" | parallel -j 56 "seqtk trimfq -b ${num3} -e ${num4} aligned/{}_aligned.fasta.gz | sed -e '/^>/s/ .*$//' -e '/^>/s/\./_/' -e '/^>/! s/U/T/g' -e '/^>/! s/-//g' > aligned_cut/V4/{}_aligned_cut.fa" 

# Concatenate all fasta files of one primerset into a single fasta file

#apply a length filter to remove bad sequences
conda activate seqkit-2.3.1 
cat V4/*.fa | seqkit seq -w 0 -m 248 -M 260 > filtered_combined_${num3}_${num4}.fa

# Set txt_file to the name of the file you want to process #### TXT FILE 13861_27656.txt #### This is a repetition of the process above for a different primer cluster
txt_file3="13861_27656.txt"

# Extract the numbers from the filename
IFS='_' read -ra AR <<< "$txt_file3"
num5=${AR[0]}
num6=$((50000 - ${AR[1]%.txt}))

# Use process substitution with parallel to read each line in the text file
cat "$txt_file3" | parallel -j 56 "seqtk trimfq -b ${num5} -e ${num6} aligned/{}_aligned.fasta.gz | sed -e '/^>/s/ .*$//' -e '/^>/s/\./_/' -e '/^>/! s/U/T/g' -e '/^>/! s/-//g' > aligned_cut/V4_V5/{}_aligned_cut.fa"  

# Concatenate all fasta files of one primerset into a single fasta file

#apply a length filter to remove bad sequences
conda activate seqkit-2.3.1 
cat V4_V5/*.fa | seqkit seq -w 0 -m 350 -M 410 > filtered_combined_${num5}_${num6}.fa

#Section G
# Cross check the Vvul database with itself by using blast and find the Vvul cluster
conda activate blast-2.15.0
blastn -query com_ref_vvul_clean.fasta -task megablast -db com_ref_vvul_clean -out vvul_db_check -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qlen gaps slen" -num_threads 50
# switch to R to visualize clusters and select sequences
#### Part 2.5 clustering of ref db ####


# Section H
### Unoise

# follow Unoise recommended approach: denoise only dominant sequences and then map reads back to zotus to create OTU table
# https://github.com/torognes/vsearch/issues/392
# https://www.nicholas-ollberding.com/post/denoising-amplicon-sequence-data-using-usearch-and-unoise3/
# https://www.drive5.com/usearch/manual/cmd_otutab.html
# https://github.com/torognes/vsearch/issues/552
# https://github.com/torognes/vsearch/issues/536
# analysis steps:

cd /bio/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/Vvul_bremen/Unoise
conda activate vsearch-2.27.0

# derplicate sequences at run level (individual fasta files)
ls -1 ../aligned_cut/V4_V5/*.fa | while read line
do
  vsearch --fastx_uniques $line --sizeout --fasta_width 0 --maxseqlength 410 --minseqlength 350 --fastaout -
done > V4_V5_unoise/in.fasta

ls -1 ../aligned_cut/V4/*.fa | while read line
do
  vsearch --fastx_uniques $line --sizeout --fasta_width 0 --maxseqlength 270 --minseqlength 230 --fastaout -
done > V4_unoise/in.fasta

# need to rename sequences for OTU table parsing later: sample ID . running number
ls -1 ../../Vvul_baltvib/aligned_cut_novaseq/*.fa | while read line
do
  SID=$(echo $line | xargs basename | sed -e 's/_aligned_cut\.fa$/./' -e 's/-/_/g')
  vsearch --fastx_uniques $line --relabel $SID --sizeout --fasta_width 0 --maxseqlength 450 --minseqlength 350 --fastaout -
done > V3_V4_unoise/in.fasta
# append V3V4 from data mined runs
ls -1 ../aligned_cut/V3_V4/*.fa | while read line
do
  vsearch --fastx_uniques $line --sizeout --fasta_width 0 --maxseqlength 450 --minseqlength 350 --fastaout -
done >> V3_V4_unoise/in.fasta
# there is still a difference in the regex for the fasta headers between ENA and BaltVib samples
# this needs to be accounted for later in parsing the sample names from the sequence names,
# but it should be managable...

# dereplicate across all runs (all fasta files)
vsearch --fastx_uniques V4_V5_unoise/in.fasta --sizein --sizeout --fasta_width 0 --fastaout V4_V5_unoise/in_nr.fasta --uc V4_V5_unoise/in_nr.uc
vsearch --fastx_uniques V4_unoise/in.fasta --sizein --sizeout --fasta_width 0 --fastaout V4_unoise/in_nr.fasta --uc V4_unoise/in_nr.uc
vsearch --fastx_uniques V3_V4_unoise/in.fasta --sizein --sizeout --fasta_width 0 --fastaout V3_V4_unoise/in_nr.fasta --uc V3_V4_unoise/in_nr.uc

# Running unoise
vsearch --cluster_unoise V4_V5_unoise/in_nr.fasta --sizein --sizeout --fasta_width 0 --minsize 10 --unoise_alpha 3 --threads 100 --centroids V4_V5_unoise/out.fasta --uc V4_V5_unoise/out.uc
vsearch --cluster_unoise V4_unoise/in_nr.fasta --sizein --sizeout --fasta_width 0 --minsize 10 --unoise_alpha 3 --threads 100 --centroids V4_unoise/out.fasta --uc V4_unoise/out.uc
vsearch --cluster_unoise V3_V4_unoise/in_nr.fasta --sizein --sizeout --fasta_width 0 --minsize 10 --unoise_alpha 3 --threads 100 --centroids V3_V4_unoise/out.fasta --uc V3_V4_unoise/out.uc

# subset in_nr.fasta to discarded sequences only (all others are already represented in out.uc), 
# also excluding singletons to speed-up assignment
vsearch --fastx_filter V4_V5_unoise/in_nr.fasta --maxsize 9 --minsize 2 --sizein --sizeout --fastaout V4_V5_unoise/in_nr_rare_nosingle.fasta
vsearch --fastx_filter V4_unoise/in_nr.fasta --maxsize 9 --minsize 2 --sizein --sizeout --fastaout V4_unoise/in_nr_rare_nosingle.fasta
vsearch --fastx_filter V3_V4_unoise/in_nr.fasta --maxsize 9 --minsize 2 --sizein --sizeout --fastaout V3_V4_unoise/in_nr_rare_nosingle.fasta

# vsearch global of all seqs < 10 to zotus
# ideally maxaccept should be set to zero to disable heuristic effects on top hit selection, but this is not feasible given the size of the data set
vsearch --usearch_global V4_V5_unoise/in_nr_rare_nosingle.fasta --db V4_V5_unoise/out.fasta --id 0.97 --threads 100 --maxaccept 1000 --maxreject 32 --top_hits_only --maxhits 1 --strand plus --uc V4_V5_unoise/out.search
vsearch --usearch_global V4_unoise/in_nr_rare_nosingle.fasta --db V4_unoise/out.fasta --id 0.97 --threads 100 --maxaccept 1000 --maxreject 32 --top_hits_only --maxhits 1 --strand plus --uc V4_unoise/out.search
vsearch --usearch_global V3_V4_unoise/in_nr_rare_nosingle.fasta --db V3_V4_unoise/out.fasta --id 0.97 --threads 100 --maxaccept 1000 --maxreject 32 --top_hits_only --maxhits 1 --strand plus --uc V3_V4_unoise/out.search

# For this output an OTU table (long format) can been parsed with: parse_unoise_otutab.R

# Initial Vvul detection test
conda activate blast-2.15.0
blastn -query V4_V5_unoise/out.fasta -task megablast -db ../Vvul_ref_clean/com_ref_vvul_clean -out V4_V5_unoise/out_Vvul.blastout -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qlen gaps staxids" -num_threads 100 -max_target_seqs 5
blastn -query V4_unoise/out.fasta -task megablast -db ../Vvul_ref_clean/com_ref_vvul_clean -out V4_unoise/out_Vvul.blastout -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qlen gaps staxids" -num_threads 100 -max_target_seqs 5
blastn -query V3_V4_unoise/out.fasta -task megablast -db ../Vvul_ref_clean/com_ref_vvul_clean -out V3_V4_unoise/out_Vvul.blastout -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qlen gaps staxids" -num_threads 100 -max_target_seqs 5

# Extracting 100% match Vvul zOTU accessions 
awk '$3 == 100 && $13 == 100' V4_V5_unoise/out_Vvul.blastout | cut -f1 | sort -u | sed 's/;size=.*$//' > V4_V5_unoise/zotu_Vvul.accnos
awk '$3 == 100 && $13 == 100' V4_unoise/out_Vvul.blastout | cut -f1 | sort -u | sed 's/;size=.*$//' > V4_unoise/zotu_Vvul.accnos
awk '$3 == 100 && $13 == 100' V3_V4_unoise/out_Vvul.blastout | cut -f1 | sort -u | sed 's/;size=.*$//' > V3_V4_unoise/zotu_Vvul.accnos

# Repeat Vvul detection test with cleaned-up database
conda activate blast-2.15.0
blastn -query V4_V5_unoise/out.fasta -task megablast -db ../Vvul_ref_updated/comb_vvul_clean_blastdb -out V4_V5_unoise/check_out_Vvul.blastout -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qlen gaps" -num_threads 64 -max_target_seqs 5
blastn -query V4_unoise/out.fasta -task megablast -db ../Vvul_ref_updated/comb_vvul_clean_blastdb -out V4_unoise/check_out_Vvul.blastout -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qlen gaps" -num_threads 64 -max_target_seqs 5
blastn -query V3_V4_unoise/out.fasta -task megablast -db ../Vvul_ref_updated/comb_vvul_clean_blastdb -out V3_V4_unoise/check_out_Vvul.blastout -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qlen gaps" -num_threads 64 -max_target_seqs 5

awk '$3 == 100 && $13 == 100' V4_V5_unoise/check_out_Vvul.blastout | cut -f1 | sort -u | sed 's/;size=.*$//' > V4_V5_unoise/check100_zotu_Vvul.accnos
awk '$3 == 100 && $13 == 100' V4_unoise/check_out_Vvul.blastout | cut -f1 | sort -u | sed 's/;size=.*$//' > V4_unoise/check100_zotu_Vvul.accnos
awk '$3 == 100 && $13 == 100' V3_V4_unoise/check_out_Vvul.blastout | cut -f1 | sort -u | sed 's/;size=.*$//' > V3_V4_unoise/check100_zotu_Vvul.accnos
# also accept hits at 98%
awk '$3 >= 98 && $13 >= 98' V4_V5_unoise/check_out_Vvul.blastout | cut -f1 | sort -u | sed 's/;size=.*$//' > V4_V5_unoise/check098_zotu_Vvul.accnos
awk '$3 >= 98 && $13 >= 98' V4_unoise/check_out_Vvul.blastout | cut -f1 | sort -u | sed 's/;size=.*$//' > V4_unoise/check098_zotu_Vvul.accnos
awk '$3 >= 98 && $13 >= 98' V3_V4_unoise/check_out_Vvul.blastout | cut -f1 | sort -u | sed 's/;size=.*$//' > V3_V4_unoise/check098_zotu_Vvul.accnos

# Blast all zotus with >=98% similarity to Vvul against redundant SILVA augmented with Vvul
grep -A1 -F -w -f V4_V5_unoise/check098_zotu_Vvul.accnos V4_V5_unoise/out.fasta | sed '/^--$/d' > V4_V5_unoise/check098_out.fasta
grep -A1 -F -w -f V4_unoise/check098_zotu_Vvul.accnos V4_unoise/out.fasta | sed '/^--$/d' > V4_unoise/check098_out.fasta
grep -A1 -F -w -f V3_V4_unoise/check098_zotu_Vvul.accnos V3_V4_unoise/out.fasta | sed '/^--$/d' > V3_V4_unoise/check098_out.fasta
# warning: these lists includes split alignments. Those should be automatically filtered out in the next blast step
blastn -query V4_V5_unoise/check098_out.fasta -task megablast -db ../Vvul_ref_updated/comb_vvul_silva_blastdb -out V4_V5_unoise/check098_out.blastout -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qlen gaps" -num_threads 120
blastn -query V4_unoise/check098_out.fasta -task megablast -db ../Vvul_ref_updated/comb_vvul_silva_blastdb -out V4_unoise/check098_out.blastout -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qlen gaps" -num_threads 120
blastn -query V3_V4_unoise/check098_out.fasta -task megablast -db ../Vvul_ref_updated/comb_vvul_silva_blastdb -out V3_V4_unoise/check098_out.blastout -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qlen gaps" -num_threads 120

# inspect all equally good best hits (based on blast score) in R: inspect_silva_vvul_blastout.R


#Prepare input files for otu_parse_taxonomy.R
cut -f1 aggregated.txt | sed '1d' | sort -u > zotu.accnos
grep -w -A1 -F -f zotu.accnos out.fasta | sed '/^--$/d' > zotu.fasta
paste <(grep '^>' zotu.fasta | sed -e 's/^>//' -e 's/;size=.*$//') <(grep -v '^>' zotu.fasta | perl -nle 'print length') > zotu_length.txt

#### Plot worldmap and apply additional coastal filter in R Global_vul_rescue.R

#### misc ####

#Count unique runs from ENA
cat sample_list.txt | cut -d '_' -f 1 | sort | uniq | wc -l #70151 + 894 BaltVib samples
#Count unique temporal dataset Victor
#148 unmerged files recieved, so 74
#416 novaseq from Baltvib
#404 miseq water samples