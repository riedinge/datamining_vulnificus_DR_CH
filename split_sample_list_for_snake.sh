#This bash file splits the sample list into single end and paired end
cat sample_list.txt | while read line
do
  if [ -f downloads/${line}.fastq.gz ]
  then
    if [ -f downloads/${line}_1.fastq.gz ]
    then
      if [ $(stat -c %s downloads/${line}.fastq.gz) -gt $(stat -c %s downloads/${line}_1.fastq.gz) ]
	  then
        # write line to list_1
        echo "${line}" >> sample_list_1.txt
      else
        # write line to list_2
        echo "${line}" >> sample_list_2.txt
	  fi
	else
	  echo "${line}" >> sample_list_1.txt  
    fi
  else
    echo "${line}" >> sample_list_2.txt
  fi
 done