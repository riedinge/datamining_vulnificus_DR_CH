#python marine database workflow
cd \Users\riedinge\copernicus_marine
#activate environments
#conda activate cmems_env_py3.9

conda activate cmems_env_py3.9
mamba activate cmt_1.0

#activate python
python
#import packages
import pandas as pd
import copernicusmarine
# In case longitude and latitude are inverted 
def sort_dimension(dataset, dim_name):
    coordinates = dataset[dim_name].values
    if (coordinates[0] >= coordinates[:-1]).all(): 
        dataset = dataset.sortby(dim_name, ascending=True)
    return dataset
	
# Read the CSV in a pandas dataframe
dataframe_coordinates = pd.read_csv("bounding_boxes_rescue_depth.csv", sep = ',')
# fix date formatting
dataframe_coordinates["collection_date"] = pd.to_datetime(dataframe_coordinates["collection_date"])
# rename lat/lon to match copernicus
dataframe_coordinates = dataframe_coordinates.rename(columns={'lat': 'Latitude', 'lon': 'Longitude', 'collection_date' : 'Date'})

list_datasetID = [
	'cmems_obs_mob_glo_phy-cur_my_0.25deg_P1D-m',
]
# Output names
output_names = [
	'ekman_current_rescue',
]
# Variables
variables = ['ue', 'uo', 've', 'vo']
# Output directory
output_dir = "Dataframes/"

# Create directory if doesn't exist
if not os.path.exists(output_dir):
	os.makedirs(output_dir)

# Loop for datasets in list_datasetID
for dataset_id, output_name in zip(list_datasetID, output_names):
    dataset = copernicusmarine.open_dataset(dataset_id = dataset_id)          
    dataset = sort_dimension(dataset, 'latitude')
    dataset = sort_dimension(dataset, 'longitude')    
    dataframe_final = dataframe_coordinates.copy()
    if "depth" in dataset.dims:
        dataframe_final = dataframe_final.assign(**{
            var : [float(dataset[var].sel(time=row[0], depth=row[3], method="nearest")\
                .sel(latitude=row[1], longitude=row[2], method='nearest'))\
                for row in zip(dataframe_final['Date'], dataframe_final['Latitude'], dataframe_final['Longitude'], dataframe_final["Depth"])]\
                    for var in variables
        })  
        # Download data for 2D datasets
    else:
        dataframe_final= dataframe_final.assign(**{
            var : [float(dataset[var].sel(time=row[0], method="nearest")\
                .sel(latitude=row[1], longitude=row[2], method='nearest'))\
                for row in zip(dataframe_final['Date'], dataframe_final['Latitude'], dataframe_final['Longitude'])]\
                for var in variables                     
        })
    dataframe_final= dataframe_final.assign(**{
        "Date_dataset" : [ dataset.sel(time=date, method="nearest").time.values for date in dataframe_final['Date'] ]
        })
    dataframe_final.to_csv(output_dir + output_name + "_temporal_points.csv")
    print("Download completed!")
#### Info from download ####
# INFO - 2024-02-26T13:08:15Z - Dataset version was not specified, the latest one was selected: "202211"
# INFO - 2024-02-26T13:08:15Z - Dataset part was not specified, the first one was selected: "default"
# INFO - 2024-02-26T13:08:16Z - Service was not specified, the default one was selected: "arco-geo-series"
#  Variables on the dataset include ['CHL', 'CHL_uncertainty', 'flags', 'latitude', 'longitude', 'time']"
#username: driedinger
#Password Jeugd maar met aanpassingen


