# To obtain 16S from whole genome sequences, download: assembly_summary_refseq.txt
cd /bio/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/Vvul_bremen/Vvul_ref_updated
wget http://ftp.ncbi.nlm.nih.gov/genomes/refseq/assembly_summary_refseq.txt

# count vulnificus
awk -v FS="\t" -v OFS="\t" '$7==672' assembly_summary_refseq.txt | wc -l # 322 hits

# establish .txt with only vulnificus
awk -v FS="\t" -v OFS="\t" '$7==672' assembly_summary_refseq.txt > assembly_summary_refseq_Vvul.txt

# find FTP link for download
head -2 assembly_summary_refseq.txt | tail -1 | tr '\t' '\n' | grep -n "ftp" #it's column 20

# make .txts with download links
cut -f20 assembly_summary_refseq_Vvul.txt | paste - <(cut -f20 assembly_summary_refseq_Vvul.txt | xargs -n1 basename | sed 's/$/_rna_from_genomic.fna.gz/') | sed 's/\t/\//' > assembly_summary_refseq_Vvul_rRNA_download.txt
# this download will give all 16S from Refseq whole genome sequences.
mkdir Vvul_refseq_genomes
cd Vvul_refseq_genomes
cat ../assembly_summary_refseq_Vvul_rRNA_download.txt | parallel -j10 wget
gunzip *.gz

# extract 16S from refseq genomes
conda activate seqkit-2.3.1
cat *.fna | seqkit seq -w 0 | grep -A1 "16S ribosomal RNA" | sed '/^--$/d' > ../vvul_refseq_genomes.fasta
cd ..

# Additionally, include all Vvul 16S also independent from whole genomes
# This search query on Refseq willö give the partial sequences with vulnificus 16S (7 references)
conda activate antismash-7.1.0
esearch -db nuccore -query "(33175[BioProject] OR 33317[BioProject]) AND txid672[Organism]" | efetch -format fasta > vvul_refseq_16s_collection.fasta

# Downloaded fixed Vibrio reference from Fernando
cd /bio/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/Vvul_bremen/Vvul_ref_updated
wget https://zenodo.org/records/10993142/files/BALTVIB_16S_rRNA_Vibrio_from_Baltic_Sea_and_RefSeq_complete_genomes_dbv1.1.fasta.gz?download=1 -O BALTVIB_16S_rRNA_Vibrio_from_Baltic_Sea_and_RefSeq_complete_genomes_dbv1.1.fasta.gz
gunzip BALTVIB_16S_rRNA_Vibrio_from_Baltic_Sea_and_RefSeq_complete_genomes_dbv1.1.fasta.gz
# extract all vulnificus
conda activate seqkit-2.3.1
seqkit seq -w 0 BALTVIB_16S_rRNA_Vibrio_from_Baltic_Sea_and_RefSeq_complete_genomes_dbv1.1.fasta | grep -A1 "vulnificus" | sed '/^--$/d' > vvul_baltvib.fasta

# Vvul counts:
# vvul_baltvib.fasta:407
# vvul_refseq_16s_collection.fasta:7
# vvul_refseq_genomes.fasta:1367

# Combine sequences, fix fasta header, and remove exact duplicates
cat vvul*fasta | sed '/^>/s/ .*$//' | seqkit rmdup -w 0 -s -D tmp_duplicated.txt > comb_vvul.fasta
# 432 unique sequence records

# Align Vvul sequences to ensure that V3-V5 regions are covered
conda activate qiime2-2021.2
sina -i comb_vvul.fasta -r /bio/Databases/SILVA/v138.1/SILVA_138.1_SSURef_NR99_12_06_20_opt.arb -o comb_vvul_aligned.fasta -t all -p 40 --meta-fmt none --overhang remove --insertion forbid --log-file comb_vvul_sina.log
# remove all seqs that don't cover V3-V5
conda activate seqtk-1.3
seqtk trimfq -b 6427 -e 22344 comb_vvul_aligned.fasta > comb_vvul_aligned_v3_v5.fasta
grep -v '^>' comb_vvul_aligned_v3_v5.fasta | sed 's/-//g' | perl -nle 'print length' | sort -k1,1g | uniq -c 
# 400bp cut: This does not guarantee that all reference cover the full V3-V5 region, but only parts thereof - which is ok.
conda activate seqkit-2.3.1
seqkit seq -w 0 -g -m 400 comb_vvul_aligned_v3_v5.fasta | grep '^>' | grep -A1 -w -F -f - comb_vvul.fasta | sed '/^--$/d' > comb_vvul_filt.fasta

# Blasting against itself to remove potential contamination
conda activate blast-2.15.0
makeblastdb -in comb_vvul_filt.fasta -dbtype nucl -out comb_vvul_filt_blastdb -logfile comb_vvul_filt_blastdb.log
blastn -query comb_vvul_filt.fasta -task megablast -db comb_vvul_filt_blastdb -out comb_vvul_filt_self.blastout -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qlen gaps" -num_threads 10
# check best hit after remove self matches in R
"""
blastout <- read.table("comb_vvul_filt_self.blastout", h = F, sep = "\t") %>% 
  filter(V1 != V2) %>% 
  group_by(V1) %>%
  arrange(desc(V12), V11) %>%
  filter(row_number() == 1) %>%
  ungroup()
hist(blastout$V3, breaks = 50, ylim = c(0, 10))
plot(blastout$V3, blastout$V13)
View(blastout[blastout$V3 < 97, ])
View(blastout[blastout$V13 < 90, ])
check_seq <- unique(blastout$V1[blastout$V3 < 97 | blastout$V13 < 90])
write(check_seq, "tmp_check.accnos")
"""
grep -A1 -w -F -f tmp_check.accnos comb_vvul_filt.fasta | sed '/^--$/d' > tmp_check.fasta

# remove outliers
"""
lcl|NZ_AMQV01000067.1_rrna_B736_RS0106805_34
lcl|NZ_LMTC01000003.1_rrna_VVS316_RS02730_3
lcl|NZ_NWBS02000090.1_rrna_COO31_RS21910_101
lcl|NZ_RBZE01000189.1_rrna_D8T51_RS23750_27
lcl|NZ_JAKNPS010000054.1_rrna_K6U59_RS12240_4
lcl|NZ_JAKNPS010000056.1_rrna_K6U59_RS12250_7
lcl|NZ_JAKNPS010000057.1_rrna_K6U59_RS12255_12
lcl|NZ_JAKNPS010000230.1_rrna_K6U59_RS15935_18
lcl|NZ_CABMOC010000004.1_rrna_FYB85_RS10215_72
lcl|NZ_AMQR01000090.1_rrna_C224_RS0108110_25
"""

conda activate seqkit-2.3.1
seqkit grep -n -f remove_contamination.txt -v comb_vvul_filt.fasta > comb_vvul_clean.fasta
# all other sequences are within 98.579% similarity to each other

# build blastDB for final Vvul selection
conda activate blast-2.15.0
makeblastdb -in comb_vvul_clean.fasta -dbtype nucl -out comb_vvul_clean_blastdb -logfile comb_vvul_clean_blastdb.log

# build generic database composed of redundant SILVA and BaltVib Virbio DB and additional (clean) Vvul refseq sequences
# fix taxonomic path in fasta header (remove spaces) and remove eukaryotes and archaea
conda activate seqkit-2.3.1
sed -e '/^>/s/ /___/' -e '/^>/s/ /_/g' /bio/Databases/SILVA/v138.1/SILVA_138.1_SSURef_tax_silva_trunc.fasta | seqkit seq -w 0 | grep -A1 "___Bacteria;" | sed '/^--$/d' | sed '/^>/! s/U/T/g' > tmp_silva.fasta
grep '^>' comb_vvul_clean.fasta | grep -v "Vibrio_vulnificus" | grep -A1 -w -F -f - <(seqkit seq -w 0 comb_vvul_clean.fasta) | sed '/^--$/d' | sed 's/^>/>refseq_Vibrio_vulnificus|/' > tmp_refseq.fasta
cat BALTVIB_16S_rRNA_Vibrio_from_Baltic_Sea_and_RefSeq_complete_genomes_dbv1.1.fasta tmp_refseq.fasta tmp_silva.fasta | seqkit seq -w 0 -m 1200 > comb_vvul_silva.fasta

conda activate blast-2.15.0
makeblastdb -in comb_vvul_silva.fasta -dbtype nucl -out comb_vvul_silva_blastdb -logfile comb_vvul_silva_blastdb.log

