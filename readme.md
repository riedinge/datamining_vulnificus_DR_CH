# Data mining project to track proliferation of V. vulnificus using ENA amplicon Data

## Summary
The project exists to prove or disprove the hypothesis that V. vulnificus is geographically spreading
To achieve this, the following steps are followed:

* Raw data collected from ENA (Search query under: Search_query_ENA.txt) (ENA website:https://www.ebi.ac.uk/ena/browser/advanced-search)

* Data filtered based on amplified region, scientific name and distance from coast (DataMining_dataCheck_GlobalVulnificus_round3.R)

* Fastq files downloaded and trimmed, merged using bbmap-39.01 and filtered (Gitbash_R_Datamining_approach.sh #Section 1)

* Reformat fastq into FASTA using reformat.sh from bbtools and BLAST (V2.13.0) the against RefSeq V. vulnificus 16S sequences (Database:xx) (Gitbash_R_Datamining_approach.sh #Section 2)

* Gather accession numbers of studies with positive hits and manually gather primer sequences (Gitbash_R_Datamining_approach.sh #Section 3)

## Contributers
* David Riedinger (ORCID:) (Linkedin: https://www.linkedin.com/in/davidriedinger/)

* Christiane Hassenrueck (ORDIC) (LinkedIn)

Under employment at the Institut für Ostseeforschung Warnemuende

## Required software

...


## Acknowledgements

..

