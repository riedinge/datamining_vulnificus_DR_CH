### unoise parameter optimization

# Running unoise
vsearch --cluster_unoise in_nr.fasta --sizein --sizeout --fasta_width 0 --minsize 100 --unoise_alpha 5 --threads 32 --centroids out.fasta --uc out.uc
# 153636394 nt in 373484 seqs, min 350, max 448, avg 411
# minsize 100: 209540734 sequences discarded.
# Clusters: 192839 Size min 100, max 14372043, avg 1.9

# For this output an OTU table has been parsed with: parse_unoise_otutab.R
# Don't use this output for the final analysis, since the minsize of 100 is not appropriate

# Testing alternative parameter settings for unois
vsearch --cluster_unoise in_nr.fasta --sizein --sizeout --fasta_width 0 --minsize 100 --unoise_alpha 3 --threads 32 --centroids out2.fasta --uc out2.uc
# 153636394 nt in 373484 seqs, min 350, max 448, avg 411
# minsize 100: 209540734 sequences discarded.
# Clusters: 170923 Size min 100, max 15120319, avg 2.2

vsearch --cluster_unoise in_nr.fasta --sizein --sizeout --fasta_width 0 --minsize 50 --unoise_alpha 3 --threads 32 --centroids out3.fasta --uc out3.uc
# 306982945 nt in 745838 seqs, min 350, max 449, avg 412
# minsize 50: 209168380 sequences discarded.
# Clusters: 311685 Size min 50, max 15228349, avg 2.4

vsearch --cluster_unoise in_nr.fasta --sizein --sizeout --fasta_width 0 --minsize 50 --unoise_alpha 5 --threads 32 --centroids out4.fasta --uc out4.uc
# 306982945 nt in 745838 seqs, min 350, max 449, avg 412
# minsize 50: 209168380 sequences discarded.
# Clusters: 354206 Size min 50, max 14461595, avg 2.1

vsearch --cluster_unoise in_nr.fasta --sizein --sizeout --fasta_width 0 --minsize 10 --unoise_alpha 5 --threads 32 --centroids out5.fasta --uc out5.uc
# 1461061500 nt in 3547315 seqs, min 350, max 450, avg 412
# minsize 10: 206366903 sequences discarded.
# Clusters: 1494623 Size min 10, max 14887829, avg 2.4

# vsearch --cluster_unoise in_nr.fasta --sizein --sizeout --fasta_width 0 --minsize 2 --unoise_alpha 5 --threads 180 --centroids out6.fasta --uc out6.uc
# killed since run time too long

vsearch --cluster_unoise in_nr.fasta --sizein --sizeout --fasta_width 0 --minsize 2 --unoise_alpha 4 --threads 80 --centroids out7.fasta --uc out7.uc
# finished on phy-2
# Clusters: 13766491 Size min 2, max 15690234, avg 2.2
# discard zOTUs with less than 100 sequences across the whole data set (10 for blasting to confirm that there are no Vibrio zotus will less than 100 sequences)
grep '^>' out7.fasta | sed -e 's/^>//' -e 's/;size=/\t/' | awk '$2 >= 100' | cut -f1 > out7.select
grep -A1 -w -F -f out7.select out7.fasta | sed '/^--$/d' > out7_filt.fasta

vsearch --cluster_unoise in_nr.fasta --sizein --sizeout --fasta_width 0 --minsize 2 --unoise_alpha 3 --threads 80 --centroids out8.fasta --uc out8.uc
# finished on olorin
# discard zOTUs with less than 100 sequences across the whole data set (10 for blasting to confirm that there are no Vibrio zotus will less than 100 sequences)
grep '^>' out8.fasta | sed -e 's/^>//' -e 's/;size=/\t/' | awk '$2 >= 100' | cut -f1 > out8.select
grep -A1 -w -F -f out8.select out8.fasta | sed '/^--$/d' > out8_filt.fasta

# Minsize 2 is the most suitable setting I think, to increase Vvul detection sensitivity
# I am not sure yet about the best alpha setting...
# I will decide on the best alpha based on the best Vvul detection sensitivity

# follow Unoise recommended approach: denoise only dominant sequences and then map reads back to zotus to create OTU table
# https://github.com/torognes/vsearch/issues/392
# https://www.nicholas-ollberding.com/post/denoising-amplicon-sequence-data-using-usearch-and-unoise3/
# https://www.drive5.com/usearch/manual/cmd_otutab.html
# https://github.com/torognes/vsearch/issues/552
# https://github.com/torognes/vsearch/issues/536
# analysis steps:

# unoise with minsize 10
vsearch --cluster_unoise in_nr.fasta --sizein --sizeout --fasta_width 0 --minsize 10 --unoise_alpha 4 --threads 60 --centroids out9.fasta --uc out9.uc
# 1461061500 nt in 3547315 seqs, min 350, max 450, avg 412
# minsize 10: 206366903 sequences discarded.
# Clusters: 1399641 Size min 10, max 14953886, avg 2.5
vsearch --cluster_unoise in_nr.fasta --sizein --sizeout --fasta_width 0 --minsize 10 --unoise_alpha 3 --threads 120 --centroids out10.fasta --uc out10.uc
# Clusters: 1289715 Size min 10, max 15692447, avg 2.8
vsearch --cluster_unoise in_nr.fasta --sizein --sizeout --fasta_width 0 --minsize 10 --unoise_alpha 2 --threads 120 --centroids out11.fasta --uc out11.uc
# Clusters: 1133972 Size min 10, max 16265697, avg 3.1

# subset in_nr.fasta to discarded sequences only (all others are already represented in out[0-9]+.uc), also excluding singletons to speed-up assignment
vsearch --fastx_filter in_nr.fasta --maxsize 9 --minsize 2 --sizein --sizeout --fastaout in_nr_rare_nosingle.fasta
# 27422365 sequences kept (of which 0 truncated), 182491853 sequences discarded. 

# vsearch global of all seqs < 10 to zotus
# ideally maxaccept should be set to zero to disable heuristic effects on top hit selection, but this is not feasible given the size of the data set
vsearch --usearch_global in_nr_rare_nosingle.fasta --db out5.fasta --id 0.97 --threads 100 --maxaccept 1000 --maxreject 32 --top_hits_only --maxhits 1 --strand plus --uc out5.search
vsearch --usearch_global in_nr_rare_nosingle.fasta --db out9.fasta --id 0.97 --threads 80 --maxaccept 1000 --maxreject 32 --top_hits_only --maxhits 1 --strand plus --uc out9.search
vsearch --usearch_global in_nr_rare_nosingle.fasta --db out10.fasta --id 0.97 --threads 110 --maxaccept 1000 --maxreject 32 --top_hits_only --maxhits 1 --strand plus --uc out10.search
vsearch --usearch_global in_nr_rare_nosingle.fasta --db out11.fasta --id 0.97 --threads 100 --maxaccept 1000 --maxreject 32 --top_hits_only --maxhits 1 --strand plus --uc out11.search

# parse OTU table
# based on final zotu abundance, remove zotus < 100
# minsize 10 alpha 5 --> 2: increasing data volume in final OTU table; highest complexity at alpha 4, lowest at alpha 2

# quick and dirty Vvul detection test
conda activate blast-2.15.0
blastn -query out9.fasta -task megablast -db ../Vvul_reference/com_ref_vvul -out test9_Vvul.blastout -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qcovs qlen gaps staxids" -num_threads 100 -max_target_seqs 1

# validate where Vvul zotus ended up in less sensitive denoising (lower alpha)

# batch with minsize 10
awk '$3 == 100 && $13 == 100' test5_Vvul.blastout | cut -f1 # 6 zotus, 5138 redundant seqs # a5
awk '$3 == 100 && $13 == 100' test9_Vvul.blastout | cut -f1 # 4 zotus, 5382 redundant seqs # a4
awk '$3 == 100 && $13 == 100' test10_Vvul.blastout | cut -f1 # 3 zotus, 5356 redundant seqs # a3
awk '$3 == 100 && $13 == 100' test11_Vvul.blastout | cut -f1 # 3 zotus, 5629 redundant seqs # a2
# SRR13317231_30253;size=156 assigned to DRR350431_125798;size=23292 (which has 1 mismatch to Vvul)

# batch with minsize 50
awk '$3 == 100 && $13 == 100' test3_Vvul.blastout | cut -f1 # 3 zotus, 4798 redundant seqs # a3
awk '$3 == 100 && $13 == 100' test4_Vvul.blastout | cut -f1 # 4 zotus, 4836 redundant seqs # a5

# batch with minsize 100
awk '$3 == 100 && $13 == 100' test_Vvul.blastout | cut -f1 # 4 zotus, 4836 redundant seqs # a5
awk '$3 == 100 && $13 == 100' test2_Vvul.blastout | cut -f1 # 3 zotus, 4680 redundant seqs # a3

# batch with minsize 2
awk '$3 == 100 && $13 == 100' test7_Vvul.blastout | cut -f1 # 4 zotus, 6582 redundant seqs, 495 Vvul positive samples # a4
awk '$3 == 100 && $13 == 100' test8_Vvul.blastout | cut -f1 # 3 zotus, 6620 redundant seqs, 504 Vvul positive samples # a3

# corrected batch with minsize 10
awk '$3 == 100 && $13 == 100' test5_Vvul.blastout | cut -f1 | grep -F -f - out5.search | cut -f9 | cut -d'=' -f2 | paste -sd+ | bc # 1386 + 5138 = 6524 redundant seqs
awk '$3 == 100 && $13 == 100' test9_Vvul.blastout | cut -f1 | grep -F -f - out9.search | cut -f9 | cut -d'=' -f2 | paste -sd+ | bc # 1435 + 5382 = 6817 redundant seqs
awk '$3 == 100 && $13 == 100' test10_Vvul.blastout | cut -f1 | grep -F -f - out10.search | cut -f9 | cut -d'=' -f2 | paste -sd+ | bc # 1454 + 5356 = 6810 redundant seqs
awk '$3 == 100 && $13 == 100' test11_Vvul.blastout | cut -f1 | grep -F -f - out11.search | cut -f9 | cut -d'=' -f2 | paste -sd+ | bc # 1569 + 5629 = 7198 redundant seqs

# in final OTU tables
awk '$3 == 100 && $13 == 100' test5_Vvul.blastout | cut -f1 | cut -d';' -f1 | grep -w -F -f - aggregated5.txt | cut -f3 | paste -sd+ | bc # 6429
awk '$3 == 100 && $13 == 100' test9_Vvul.blastout | cut -f1 | cut -d';' -f1 | grep -w -F -f - aggregated9.txt | cut -f3 | paste -sd+ | bc # 6817
awk '$3 == 100 && $13 == 100' test10_Vvul.blastout | cut -f1 | cut -d';' -f1 | grep -w -F -f - aggregated10.txt | cut -f3 | paste -sd+ | bc # 6810
awk '$3 == 100 && $13 == 100' test11_Vvul.blastout | cut -f1 | cut -d';' -f1 | grep -w -F -f - aggregated11.txt | cut -f3 | paste -sd+ | bc # 7198

# number of positive samples
awk '$3 == 100 && $13 == 100' test5_Vvul.blastout | cut -f1 | cut -d';' -f1 | grep -w -F -f - aggregated5.txt | cut -f2 | sort | uniq | wc -l # 486
awk '$3 == 100 && $13 == 100' test9_Vvul.blastout | cut -f1 | cut -d';' -f1 | grep -w -F -f - aggregated9.txt | cut -f2 | sort | uniq | wc -l # 511
awk '$3 == 100 && $13 == 100' test10_Vvul.blastout | cut -f1 | cut -d';' -f1 | grep -w -F -f - aggregated10.txt | cut -f2 | sort | uniq | wc -l # 513
awk '$3 == 100 && $13 == 100' test11_Vvul.blastout | cut -f1 | cut -d';' -f1 | grep -w -F -f - aggregated11.txt | cut -f2 | sort | uniq | wc -l # 545

