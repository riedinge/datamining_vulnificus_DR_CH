# Prepare download (on olorin)
cd /storage/hdd1/chh/IOWseq000045/download_subsets
split -l 1000 <(sed '1d' ../coastal_tsv_vibrio.tsv)
ls -1 x* | while read line
do
  cut -f21 $line | tr ';' '\n' | sed 's/^/ftp:\/\//' > download_${line}.txt
done

# Download with aria2
conda activate aria2
cd /storage/hdd1/chh/IOWseq000045/downloaded
aria2c -i ../download_subsets/download_xaa.txt -c --dir . --max-tries=20 --retry-wait=5 --max-connection-per-server=1 --max-concurrent-downloads=50 &>> xaa.log
aria2c -i ../download_subsets/download_xab.txt -c --dir . --max-tries=20 --retry-wait=5 --max-connection-per-server=1 --max-concurrent-downloads=50 &>> xab.log
# the download seems to be stable, so I am increasing the batch size
cat ../download_subsets/download_xa[c-z].txt > ../download_subsets/download_xa_combined.txt
cat ../download_subsets/download_xb[a-z].txt > ../download_subsets/download_xb_combined.txt
cat ../download_subsets/download_xc[a-z].txt > ../download_subsets/download_xc_combined.txt
aria2c -i ../download_subsets/download_xa_combined.txt -c --dir . --max-tries=20 --retry-wait=5 --max-connection-per-server=1 --max-concurrent-downloads=10 &>> xa_combined.log
aria2c -i ../download_subsets/download_xb_combined.txt -c --dir . --max-tries=20 --retry-wait=5 --max-connection-per-server=1 --max-concurrent-downloads=10 &>> xb_combined.log
aria2c -i ../download_subsets/download_xc_combined.txt -c --dir . --max-tries=20 --retry-wait=5 --max-connection-per-server=1 --max-concurrent-downloads=10 &>> xc_combined.log
# check md5sums
cut -f21 ../coastal_tsv_vibrio.tsv | sed '1d' | tr ';' '\n' | xargs -n1 basename | paste <(cut -f22 ../coastal_tsv_vibrio.tsv | sed '1d' | tr ';' '\n') - > md5sums.txt
md5sum -c md5sums.txt > md5sums_checked.txt
# several failed
grep "FAILED" md5sums_checked.txt | cut -d':' -f1 | grep -c -F -f - ../download_subsets/download_xaa.txt
grep "FAILED" md5sums_checked.txt | cut -d':' -f1 | grep -c -F -f - ../download_subsets/download_xab.txt
grep "FAILED" md5sums_checked.txt | cut -d':' -f1 | grep -c -F -f - ../download_subsets/download_*_combined.txt
grep "FAILED" md5sums_checked.txt | cut -d':' -f1 | grep -F -f - ../download_subsets/download_*_combined.txt | sed 's/^.*_combined.txt://' > ../download_subsets/download_repeat1.txt
grep "FAILED" md5sums_checked.txt | cut -d':' -f1 | parallel -j20 rm
aria2c -i ../download_subsets/download_repeat1.txt -c --dir . --max-tries=20 --retry-wait=5 --max-connection-per-server=1 --max-concurrent-downloads=10 &>> download_repeat1.log
cat ../download_subsets/download_repeat1.txt | xargs -n1 basename | grep -F -f - md5sums.txt > md5sums_repeat1.txt
md5sum -c md5sums_repeat1.txt > md5sums_checked_repeat1.txt
grep "FAILED" md5sums_checked_repeat1.txt | cut -d':' -f1 | grep -F -f - ../download_subsets/download_*_combined.txt | sed 's/^.*_combined.txt://' > ../download_subsets/download_repeat2.txt
aria2c -i ../download_subsets/download_repeat2.txt -c --dir . --max-tries=20 --retry-wait=5 --max-connection-per-server=1 --max-concurrent-downloads=10 &>> download_repeat2.log
# now all should be ok

# transfer to HLRN
cd ..
scp -r downloaded/ glogin:/scratch/usr/mvbchass/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/downloads

# split sample list into already merged and paired-end samples
cut -f38 coastal_tsv_vibrio.tsv | sed '1d' | while read line
do
  if [ -f downloads/${line}.fastq.gz ]
  then
    if [ -f downloads/${line}_1.fastq.gz ]
    then
      if [ $(stat -c %s downloads/${line}.fastq.gz) -gt $(stat -c %s downloads/${line}_1.fastq.gz) ]
	  then
        # write line to list_1
        echo "${line}" >> sample_list_1.txt
      else
        # write line to list_2
        echo "${line}" >> sample_list_2.txt
	  fi
	else
	  echo "${line}" >> sample_list_1.txt  
    fi
  else
    echo "${line}" >> sample_list_2.txt
  fi
 done
# move sample lists to assets of repo

# testing snakes
conda activate /scratch/usr/mvbchass/.conda/snakemake-env
cd /scratch/usr/mvbchass/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/Repos/datamining_vulnificus_DR_CH/snakemake_wf

# already merged files (sample list 1)
snakemake -s Snakefile_trim_merged_single_files --use-conda -j 100 --cluster-config config/slurm_config.yaml --cluster "sbatch --export=ALL --no-requeue -t {cluster.time} -N {cluster.nodes} -p {cluster.partition} -c {threads} -q {cluster.qos} -A {cluster.account} -J {rulename}.{jobid}" -np
# prepare conda envs before 'real' job execution
snakemake -s Snakefile_trim_merged_single_files --use-conda --conda-create-envs-only --cores 1

# trim and merge PE files (sample list 2)
snakemake -s Snakefile_trim_merge_paired_files --use-conda -j 10 --cluster-config config/slurm_config.yaml --cluster "sbatch --export=ALL --no-requeue -t {cluster.time} -N {cluster.nodes} -p {cluster.partition} -c {threads} -q {cluster.qos} -A {cluster.account} -J {rulename}.{jobid}" --keep-going -np
# for easier processing, split sample list into batches of 5000
split -l 5000 sample_list_2.txt

# problematic samples (removed for now)
SRR18579003
SRR18707541
SRR22852269
SRR22851869

# blast at HLRN
# also split sample list into batches of 5000
snakemake -s Snakefile_megablast --use-conda -j 10 --cluster-config config/slurm_config.yaml --cluster "sbatch --export=ALL --no-requeue -t {cluster.time} -N {cluster.nodes} -p {cluster.partition} -c {threads} -q {cluster.qos} -A {cluster.account} -J {rulename}.{jobid}" --keep-going -np

# in case the queue is too busy, run the short jobs on the login nodes
# in general, short jobs should not be submitted to the queue
cd /scratch/usr/mvbchass/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/fasta_files
conda activate /scratch-emmy/usr/mvbchass/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/Repos/datamining_vulnificus_DR_CH/snakemake_wf/.snakemake/conda/0c4a428cbcf4cbcf8871e37f7a6acd19
ls -1 | cut -d'.' -f1 | grep -w -F -v -f - ../Repos/datamining_vulnificus_DR_CH/snakemake_wf/assets/xad | parallel -j5 'reformat.sh in=../merged/{}.fastq.gz out={}.fa fastawrap=10000 threads=1 > ../logs_blast/{}_reformat.log 2>&1'
conda deactivate
# better yet: run with queue but only in 1 step
cd /scratch/usr/mvbchass/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/Repos/datamining_vulnificus_DR_CH/snakemake_wf
sbatch ./fastq_to_fasta.sh xae
# blast filtering is ok without queue
cd /scratch/usr/mvbchass/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/blast_filt
ls -1 | cut -d'_' -f1 | grep -w -F -v -f - ../Repos/datamining_vulnificus_DR_CH/snakemake_wf/assets/xac | parallel -j10 "awk '\$3 >= 95 && \$13 >= 95' ../blast_out/{}_blast_out.txt > {}_blast_filt.txt"

# run sina at HLRN
# when running multiple small jobs, group them in small batches to be run in parallel one node (be careful to partition the threads accordingly)
# when grouping jobs, the cluster config are not respected
# first, finish all sina runs, then all parsing jobs... I am not sure how best to deal with dependencies in this case, so I am running the snake without any
snakemake -s Snakefile_sina --use-conda -j 50 --groups run_sina=group0 --group-components group0=8 --cluster "sbatch --export=ALL --no-requeue -t 720 -N 1 -p medium40 -c 80 -q preempt -A mvk00096 -J {rulename}.{jobid}" --keep-going -np
snakemake -s Snakefile_sina --use-conda -j 50 --groups parse=group0 --group-components group0=8 --cluster "sbatch --export=ALL --no-requeue -t 720 -N 1 -p medium40 -c 80 -q preempt -A mvk00096 -J {rulename}.{jobid}" --keep-going -np

# process BaltVib samples (similar to ENA obtained samples
snakemake -s Snakefile_trim_merge_miseq_baltvib --use-conda -j 50 --groups trim_paired_end=group0 merge_paired_end=group0 fastq_to_fasta=group0 run_sina=group0 --cluster "sbatch --export=ALL --no-requeue -t 720 -N 1 -p medium40 -c 80 -q preempt -A mvk00096 -J {rulename}.{jobid}" -np

snakemake -s Snakefile_merge_trim_novaseq --use-conda -j 20 --groups merge_paired_end=group0 trim_single_end=group0 fastq_to_fasta=group0 run_sina=group0 --cluster "sbatch --export=ALL --no-requeue -t 720 -N 1 -p medium40 -c 80 -q preempt -A mvk00096 -J {rulename}.{jobid}" -np
# running outside of preempt queue
snakemake -s Snakefile_merge_trim_novaseq --use-conda -j 20 --groups merge_paired_end=group0 trim_single_end=group0 fastq_to_fasta=group0 run_sina=group0 --cluster "sbatch --export=ALL --no-requeue -t 720 -N 1 -p standard96 -c 192 -A mvbchass -J {rulename}.{jobid}" -np

### error in merging L75
# repeat merging for ENA runs downloaded as separate R1 and R2 files
snakemake -s Snakefile_trim_merge_paired_files --use-conda -j 10 --groups merge_paired_end=group0 --group-components group0=32 --cluster "sbatch --export=ALL --no-requeue -t 720 -N 1 -p standard96 -c 192 -A mvbchass -J {rulename}.{jobid}" -np

# repeat blast screening for those samples that were additionally selected with updated bbmerge settings
snakemake -s Snakefile_megablast --use-conda -j 10 --groups run_blast=group0 --group-components group0=10 --cluster "sbatch --export=ALL --no-requeue -t 720 -N 1 -p medium40 -c 80 -q preempt -A mvk00096 -J {rulename}.{jobid}" -np

# run sina for additional runs and repeat sina for those that were remerged
# additional runs with few sequences (use group jobs)
snakemake -s Snakefile_sina --use-conda -j 50 --groups run_sina=group0 --group-components group0=8 --cluster "sbatch --export=ALL --no-requeue -t 720 -N 1 -p medium40 -c 80 -q preempt -A mvk00096 -J {rulename}.{jobid}" -np
snakemake -s Snakefile_sina --use-conda -j 50 --groups parse=group0 --group-components group0=8 --cluster "sbatch --export=ALL --no-requeue -t 720 -N 1 -p medium40 -c 80 -q preempt -A mvk00096 -J {rulename}.{jobid}" -np
# additional runs with many sequences (processes individually)
snakemake -s Snakefile_sina --use-conda -j 50 --cluster "sbatch --export=ALL --no-requeue -t 720 -N 1 -p medium40 -c 80 -q preempt -A mvk00096 -J {rulename}.{jobid}" -np
# parse large runs outside of snakemake (only processing the first 100000 records), since snakemake keeps throwing an exit code error that should not exist, the bash code of the rule works perfectly

# repeat runs with few sequences (use group jobs)
snakemake -s Snakefile_sina --use-conda -j 50 --groups run_sina=group0 --group-components group0=8 --cluster "sbatch --export=ALL --no-requeue -t 720 -N 1 -p medium40 -c 80 -q preempt -A mvk00096 -J {rulename}.{jobid}" -np
# repeat runs with many sequences (processes individually)
snakemake -s Snakefile_sina --use-conda -j 50 --cluster "sbatch --export=ALL --no-requeue -t 720 -N 1 -p medium40 -c 80 -q preempt -A mvk00096 -J {rulename}.{jobid}" -np


