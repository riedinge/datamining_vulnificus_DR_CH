#!/bin/bash

#SBATCH -t 720
#SBATCH -N 1
#SBATCH -p medium40
#SBATCH -A mvk00096
#SBATCH --qos preempt
#SBATCH --export=ALL
#SBATCH -c 80
#SBATCH -J parse

SID=$1

source ~/.bashrc
cd /scratch/usr/mvbchass/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/parsed

grep -F "[log] [info]" ../logs_alignment/${SID}_alinged.log | sed '1d' | sed 's/^.*\[log\] \[info\] //' | head -1500000 | parallel -j80 -N1 --recstart 'sequence_number' --pipe "tr '\n' '\t' | sed 's/$/\n/'" > ${SID}_parsed.txt

