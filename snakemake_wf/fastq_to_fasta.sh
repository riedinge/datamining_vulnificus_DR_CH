#!/bin/bash

#SBATCH -t 720
#SBATCH -N 1
#SBATCH -p standard96
#SBATCH -A mvbchass
#SBATCH --export=ALL
#SBATCH -c 96
#SBATCH -J fastq_to_fasta

BATCH=$1

source ~/.bashrc
cd /scratch/usr/mvbchass/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/fasta_files
conda activate /scratch-emmy/usr/mvbchass/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/Repos/datamining_vulnificus_DR_CH/snakemake_wf/.snakemake/conda/0c4a428cbcf4cbcf8871e37f7a6acd19
ls -1 | cut -d'.' -f1 | grep -w -F -v -f - ../Repos/datamining_vulnificus_DR_CH/snakemake_wf/assets/$BATCH | parallel -j48 'reformat.sh in=../merged/{}.fastq.gz out={}.fa fastawrap=10000 threads=1 > ../logs_blast/{}_reformat.log 2>&1'


