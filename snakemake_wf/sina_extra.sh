#!/bin/bash

#SBATCH -t 1440
#SBATCH -N 1
#SBATCH -p standard96
#SBATCH -A mvbchass
#SBATCH --export=ALL
#SBATCH -c 192
#SBATCH --qos 24h
#SBATCH -J sina

SID=$1

source ~/.bashrc
conda activate /scratch-emmy/usr/mvbchass/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/Repos/datamining_vulnificus_DR_CH/snakemake_wf/.snakemake/conda/4958830dfdb5531623e81cd65867bb32
cd /scratch/usr/mvbchass/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/
sina -i fasta_files/${SID}.fa -r /scratch/fast/usr/mvbchass/Databases/SILVA/v138.1/SILVA_138.1_SSURef_NR99_12_06_20_opt.arb -o aligned/${SID}_aligned.fasta -t all -p 192 --meta-fmt header --overhang remove --insertion forbid --log-file logs_alignment/${SID}_alinged.log



