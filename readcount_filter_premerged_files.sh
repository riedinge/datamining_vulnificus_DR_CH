#Check the premerged files reads # Many 0 or very low
(blast-2.13.0) riedinge@bio-40:/bio/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/Vvul_bremen/logs_single$ grep "Result" *_trimming.log 

#Filter sample_list1 (premerged) for having at least 10000 reads. # 3640 becomes 2230 files
for file in *_trimming.log; do
    grep "Result" "$file" | sed "s/^/${file}\t/"
done | sed -r 's/\s+/\t/g' | sed 's/_trimming.log//' | cut -f1,3,5,6,8 | sed -e 's/(//g' -e 's/)//g' -e 's/%//g' > ../trimming_stats_single.txt
# col 2+3: reads; col 4+5: bases
cd ..
awk '$2 >= 10000' trimming_stats_single.txt > samplelist1_filtered.txt

#Check the mergend and trimmed files reads # Many 0 or very low
riedinge@bio-40:/bio/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/Vvul_bremen/logs_paired$ grep "Result" *_trimming.log 
# argumentlist too long, write as forloop:
for file in *_trimming.log; do
    grep "Result" "$file" | sed "s/^/${file}\t/"
done | sed -r 's/\s+/\t/g' | sed 's/_trimming.log//' | cut -f1,3,5,6,8 | sed -e 's/(//g' -e 's/)//g' -e 's/%//g' > ../trimming_stats_paired.txt

for file in *_merged.log; do
    grep "Joined" "$file" | sed "s/^/${file}\t/"
done | sed -r 's/\s+/\t/g' | sed 's/_merged.log//' | cut -f1,3,4 | sed 's/%//' > ../merged_stats_paired.txt
# only numbers ofr reads and not bases included


# export all stats to R and continue at "Part 2 - stats_trimmed_merged" in DataMining_dataCheck_GlobalVulnificus_git.R

scp riedinge@bio-40:/bio/Analysis_data/IOWseq000045_BaltVib/Intermediate_results/Vvul_bremen/trimming_stats_single.txt /c/Users/riedinge/DataMiningCHR/Round3/ #and merged_stats_paired.txt & trimming_stats_paired.txt
